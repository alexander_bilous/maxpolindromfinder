/**
 * Created by billp on 22.02.2017.
 */
public class LargestPolindrom {

    private int largestPolindrome;

    public LargestPolindrom() {
        largestPolindrome = 0;
    }

    public void findLargestPolindrome() {

        int tempValue = 0;
        for (int i = 999; i >= 800; --i) {
            for (int j = 999; j > i; --j) {
                tempValue = i * j;
                if (tempValue > this.largestPolindrome && isPolindrome(tempValue)) {
                    this.largestPolindrome = tempValue;
                }
            }
        }
    }

    public boolean isPolindrome(int _value) {
        String value = String.valueOf(_value);
        return value.equals(new StringBuilder(value).reverse().toString());
    }

    public int getLargestPolindrome() {
        return largestPolindrome;
    }
}
