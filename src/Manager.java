/**
 * Created by billp on 22.02.2017.
 */
public class Manager {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        LargestPolindrom largestPolindrom = new LargestPolindrom();
        largestPolindrom.findLargestPolindrome();
        long time = System.currentTimeMillis() - startTime;
        System.out.println("Max polindrom: " + largestPolindrom.getLargestPolindrome());
        System.out.println("Process time: " + time + "ms");
    }
}
